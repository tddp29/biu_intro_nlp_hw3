from word2id import Word2ID
from collections import Counter
import preprocess
from filters import MultiFilters, ThresholdWordFilter, PunctuationFilter, get_punctuations, DummyFilter, threshold_word_context_coocc
from coocc_models import FullContextCoOcc, SkipFunctionWordsWindowContextCoOc, get_function_words, DependencyContextCoOcc
import word2vec_utils
import pandas as pd


def calc_coocc_similarity(input_file, words, options):
    alpha = options["alpha"]
    min_word_count = options["min_word_count"]
    min_context_count = options["min_context_count"]

    word2id = Word2ID()
    word_count = Counter()

    # load sentences
    print("reading sentences...", end='', flush=True)
    with open(input_file, "r", encoding="utf-8") as f:
        sentences = preprocess.read_all_sentences(f.read(), word2id)
    print("done")

    # calc word count
    print("calculating word count...", end='', flush=True)
    for sentence in sentences:
        for word in sentence:
            word_count[word.id] += 1
    print("done")

    # filters
    punct_filter = PunctuationFilter(get_punctuations(word2id))
    word_threshold_filter = ThresholdWordFilter(word_count, min_word_count)
    context_threshold_filter = ThresholdWordFilter(
        word_count, min_context_count)

    word_filter = MultiFilters((word_threshold_filter, punct_filter))
    context_filter = MultiFilters((context_threshold_filter, punct_filter))

    # calc cooccs
    full_context_coocc = FullContextCoOcc(word_filter, context_filter)
    window_context_coocc = SkipFunctionWordsWindowContextCoOc(
        word_filter, context_filter, get_function_words(word2id))
    dependency_context_coocc = DependencyContextCoOcc(
        word_filter, DummyFilter())

    calc_single_coocc(word2id, full_context_coocc, "full", sentences, options)
    calc_single_coocc(word2id, window_context_coocc,
                      "window", sentences, options)
    calc_single_coocc(word2id, dependency_context_coocc,
                      "dependency", sentences, options)

    # calc word vectors
    print("calculating word vectors from cooccs...", end='', flush=True)
    full_context_coocc.calc_vectors(alpha)
    window_context_coocc.calc_vectors(alpha)
    dependency_context_coocc.calc_vectors(alpha)
    print("done")

    for word in words:
        for similarity_type in ("second_order_similarity", "first_order_similarity"):
            df = calc_coocc_word_similarity(word, word2id, similarity_type,
                                            full_context_coocc, window_context_coocc, dependency_context_coocc)

            print(word, similarity_type)
            print("=" * 80)
            with pd.option_context('display.max_rows', None, 'display.max_columns', None):
                print(df)
            print("=" * 80)


def calc_single_coocc(word2id, coocc, coocc_name, sentences, options):
    min_word_context_count = options["min_word_context_count"]

    print("calculating co-occ for {}...".format(coocc_name), end='', flush=True)
    for sentence in sentences:
        coocc.add_sentence(sentence)
    print("done")

    print("coocc size: {} words, {} features in total".format(
        *get_coocc_size(coocc)))

    print("thresholding word-context coocc count to min {}...".format(
        min_word_context_count, end='', flush=True))
    coocc.word_context_count, coocc.words_having_context = threshold_word_context_coocc(
        coocc.word_context_count, min_word_context_count)
    print("done")

    print("coocc size: {} words, {} features in total".format(
        *get_coocc_size(coocc)))


def calc_coocc_word_similarity(word, word2id, similarity_type, full_context_coocc, window_context_coocc, dependency_context_coocc):
    r = []
    if similarity_type == "first_order_similarity":
        r.append(full_context_coocc.most_activating_contexts(
            word, word2id, top_n=10))
        r.append(window_context_coocc.most_activating_contexts(
            word, word2id, top_n=10))
        r.append(dependency_context_coocc.most_activating_contexts(
            word, word2id, top_n=10))
    elif similarity_type == "second_order_similarity":
        r.append(full_context_coocc.most_similar_words(
            word, word2id, top_n=20))
        r.append(window_context_coocc.most_similar_words(
            word, word2id, top_n=20))
        r.append(dependency_context_coocc.most_similar_words(
            word, word2id, top_n=20))
    else:
        raise Exception(
            "unknown type of similarity {}".format(similarity_type))

    table = []
    for ((w1, s1), (w2, s2), (w3, s3)) in zip(*r):
        table.append((w1, s1, w2, s2, w3, s3))

    df = pd.DataFrame(table, columns=[
        'word', 'full_score',
        'word', 'window_score',
        'word', 'dependency_score'])

    return df


def calc_word2vec_similarity(bow_words_input_file, bow_contexts_input_file,
                             dep_words_input_file, dep_contexts_input_file, words):
    print("word2vec bow")
    calc_single_word2vec_similarity(
        bow_contexts_input_file, bow_contexts_input_file, words)

    print("=" * 80 + "\n")

    print("word2vec dependency")
    calc_single_word2vec_similarity(
        dep_contexts_input_file, dep_contexts_input_file, words)


def calc_single_word2vec_similarity(words_input_file, contexts_input_file, words):
    print("loading words and contexts models...", end='', flush=True)
    words_matrix, words_i2w, words_w2i = word2vec_utils.load_model(
        words_input_file)
    contexts_matrix, contexts_i2c, contexts_c2i = word2vec_utils.load_model(
        contexts_input_file)
    print("done")

    for word in words:
        df = calc_word2vec_word_similarity(word,
                                           words_matrix, words_w2i, words_i2w,
                                           contexts_matrix, contexts_i2c)

        print(word)
        print("=" * 80)
        print(df)
        print("=" * 80)


def calc_word2vec_word_similarity(word, words_matrix, w2i, i2w, contexts_matrix, i2c):
    word_vec = words_matrix[w2i[word]]

    r1 = word2vec_utils.word2vec_most_similar_words(
        words_matrix, i2w, word_vec)
    r2 = word2vec_utils.word2vec_most_activating_contexts(
        contexts_matrix, i2c, word_vec)
    # fill empty ('', '') to complete table
    r2 += [('', '')] * 10

    r = []
    for ((w1, s1), (w2, s2)) in zip(r1, r2):
        r.append((w1, s1, w2, s2))

    df = pd.DataFrame(
        r, columns=['word', 'similar_word', 'word', 'activating_contexts'])
    return df


def get_coocc_size(coocc):
    return len(coocc.word_context_count), sum((len(contexts) for _, contexts in coocc.word_context_count.items()))

from collections import defaultdict, Counter
from pmi import pmi
import math


def get_vector(word_context_count, words_having_context, word, alpha=1):
    '''
    get a single vector for word
    alpha is a smoothing factor of P(w)
    returns vector, norm
    '''
    vec = {}
    norm = 0

    allcnt = all_w_c_count(word_context_count, alpha)
    wcnt = w_count(word_context_count, word, alpha)

    for context, word_context_countnt in word_context_count[word].items():
        ccnt = c_count(word_context_count, words_having_context, context)
        ppmi = max(0, pmi(allcnt, word_context_countnt, wcnt, ccnt))
        vec[context] = ppmi
        norm += ppmi**2

    norm = math.sqrt(norm)

    return vec, norm


def get_vectors(word_context_count, words_having_context, alpha=1):
    '''
    calc all vectors from word_context_count
    alpha is a smoothing factor of P(w)
    returns vector, norm
    '''
    allcnt = 0
    words_count = Counter()
    contexts_count = Counter()

    # calc words_count and context_count
    for word in word_context_count:
        for context, cnt in word_context_count[word].items():
            words_count[word] += cnt
            contexts_count[context] += cnt
        words_count[word] **= alpha
        allcnt += words_count[word]

    word_vectors = defaultdict(Counter)
    norms = Counter()

    # calc vectors and norms
    for word in word_context_count:
        wcnt = words_count[word]
        for context, cnt in word_context_count[word].items():
            ppmi = max(0, pmi(allcnt, cnt, wcnt, contexts_count[context]))
            word_vectors[word][context] = ppmi
            norms[word] += ppmi**2
        norms[word] = math.sqrt(norms[word])

    return word_vectors, norms

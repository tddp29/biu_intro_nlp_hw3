from collections import defaultdict, Counter


class WordFilter(object):
    def filter(self, word):
        raise NotImplementedError()


class MultiFilters(WordFilter):
    '''
    filter = True if all multiple filter = True (AND operator)
    '''
    def __init__(self, filters):
        self._filters = filters

    def filter(self, word):
        return all((f.filter(word) for f in self._filters))


class ThresholdWordFilter(WordFilter):
    '''
    filters word with word_count > min_word_count
    '''
    def __init__(self, word_count, min_word_count):
        self._word_count = word_count
        self._min_word_count = min_word_count

    def filter(self, word):
        return self._word_count[word.id] >= self._min_word_count


PUNCTUATIONS = ['"', '.', ',', ':', ';',
                '(', "'", ')', '[', ']', '{', '}', '`', '?', '!', "''", '``']


def get_punctuations(word2id):
    return set((word2id.get_id_by_word(word) for word in PUNCTUATIONS))


class PunctuationFilter(WordFilter):
    '''
    filters punctuation words
    '''
    def __init__(self, punctuations):
        self.punctuations = punctuations

    def filter(self, word):
        return word.id not in self.punctuations


def threshold_word_context_coocc(word_context_count, min_word_context_count):
    '''
    keep word_context_count values >= min_word_context_count
    '''
    wcc_mini, whc_mini = defaultdict(Counter), defaultdict(set)
    for word in word_context_count:
        for context, cnt in word_context_count[word].items():
            if cnt >= min_word_context_count:
                wcc_mini[word][context] = cnt
                whc_mini[context].add(word)
    return wcc_mini, whc_mini


class DummyFilter(object):
    '''
    filter = True
    '''
    def filter(self, word):
        return True

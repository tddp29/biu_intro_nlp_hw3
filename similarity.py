import math
from collections import Counter


def cosine_vectors(v1, v2, top_n=5):
    '''
    calc second order similarity between v1 and v2
    returns similarity measure and the top_n strongest feature
    used in order to understand which contexts activates both v1 and v2 the most
    '''
    c1 = set(v1.keys())
    c2 = set(v2.keys())

    dot_prod = 0
    features = []

    for c in c1.intersection(c2):
        v = v1[c] * v2[c]
        dot_prod += v
        features.append((c, v))

    return dot_prod, sorted(features, key=lambda t: t[1], reverse=True)[:top_n]


def coocc_first_order_similarity(word_vector, top_n=20):
    '''
    calc first order similarity - top_n c argmax(w * c) for c in w
    '''
    return sorted(word_vector.items(), key=lambda t: t[1], reverse=True)[:top_n]


def coocc_second_order_similarity(word_vectors, norms, words_having_context, word, top_n=20):
    '''
    calc second order similarity - top_n argmax(w * w') for all w' in word_vectors
    '''
    dot_prod = Counter()

    word_vector = word_vectors[word]

    for context in word_vector:
        for other_word in words_having_context[context]:
            other_word_vector = word_vectors[other_word]
            dot_prod[other_word] += word_vector[context] * \
                other_word_vector[context]

    for other_word in dot_prod:
        dot_prod[other_word] /= norms[word] * norms[other_word]

    return sorted(dot_prod.items(), key=lambda t: t[1], reverse=True)[:top_n]

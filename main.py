import sys
from utils import calc_coocc_similarity

# PMI smoothing factor
ALPHA = 0.75
# filter words having word_count >= MIN_WORD_COUNT
MIN_WORD_COUNT = 5
# filter contexts having word_count >= MIN_CONTEXT_COUNT
MIN_CONTEXT_COUNT = 5
# filter words-context values >= MIN_WORD_CONTEXT_COUNT
MIN_WORD_CONTEXT_COUNT = 1


def main(coocc_input_file,
         bow_words_input_file,
         bow_contexts_input_file,
         dep_words_input_file,
         dep_contexts_input_file):
    options = {
        "alpha": ALPHA,
        "min_word_count": MIN_WORD_COUNT,
        "min_context_count": MIN_CONTEXT_COUNT,
        "min_word_context_count": MIN_WORD_CONTEXT_COUNT
    }

    words = ['car', 'piano', 'bus', 'hospital', 'hotel', 'gun',
             'bomb', 'horse', 'fox', 'table', 'bowl', 'guitar']

    calc_coocc_similarity(coocc_input_file, words, options)

    calc_word2vec_similarity(bow_words_input_file, bow_contexts_input_file,
                             dep_words_input_file, dep_contexts_input_file, words)

if __name__ == "__main__":
    if len(sys.argv) < 6:
        print(
            "usage: python main.py [coocc_input_file] [bow_words_input_file] [bow_contexts_input_file] [dep_words_input_file] [dep_contexts_input_file]")
        exit(1)

    coocc_input_file = sys.argv[1]
    bow_words_input_file = sys.argv[2]
    bow_contexts_input_file = sys.argv[3]
    dep_words_input_file = sys.argv[4]
    dep_contexts_input_file = sys.argv[5]

    main(coocc_input_file,
         bow_words_input_file,
         bow_contexts_input_file,
         dep_words_input_file,
         dep_contexts_input_file)

class Word2ID(object):
    '''
    map word <-> id (both ways)
    '''

    def __init__(self):
        self._word2id = {}
        self._id2word = {}

    def get_id_by_word(self, word):
        '''
        returns corresponding id of word
        creates a new id for word if doesn't exist yet
        '''
        if word not in self._word2id:
            next_id = len(self._word2id)
            self._word2id[word] = next_id
            self._id2word[next_id] = word

        return self._word2id[word]

    def get_word_by_id(self, w_id):
        '''
        returns word corresponding to id
        '''
        return self._id2word[w_id]

    def get_size(self):
        return len(self._word2id)

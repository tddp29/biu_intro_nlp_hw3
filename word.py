EMPTY_SYMBOL = '_'
NO_HEAD = '0'
NO_DEP = 'ROOT'
FORM_I = 1
LEMMA_I = 2
POSTAG_I = 4
HEAD_I = 6
DEPREL_I = 7
# Penn Treebank postag: IN - Preposition or subordinating conjunction
PREPOSITION_POSTAG = 'IN'
# Penn Treebank postag: all noun forms
NOUN_POSTAGS = set(('NN', 'NNS', 'NNP', 'NNPS'))


class Word(object):
    '''
    simple struct-like of the word's properties
    described in https://depparse.uvt.nl/DataFormat.html
    '''

    def __init__(self, word_props, word2id):
        lemma = word_props[LEMMA_I] if word_props[
            LEMMA_I] != EMPTY_SYMBOL else word_props[FORM_I]
        self.id = word2id.get_id_by_word(lemma)
        self.postag = word_props[POSTAG_I]
        self.head = None if word_props[HEAD_I] == NO_HEAD else int(word_props[
                                                                   HEAD_I]) - 1
        self.deprel = word_props[DEPREL_I] if word_props[
            DEPREL_I] != NO_DEP else None

    def is_preposition(self):
        return self.postag == PREPOSITION_POSTAG

    def is_noun(self):
        return self.postag in NOUN_POSTAGS

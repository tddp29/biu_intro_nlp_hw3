from word import Word


def read_all_sentences(file_content, word2id, word_separator='\t', line_separator='\n'):
    '''
    parses all sentences out of file_content
    '''
    sentence = []
    sentences = []
    for line in file_content.split(line_separator):
        if len(line) == 0:
            sentences.append(make_sentence(sentence, word2id))
            sentence = []
        else:
            word_props = line.split(word_separator)
            sentence.append(word_props)

    # last sentence
    if len(sentence) > 0:
        sentences.append([Word(word_props, word2id)
                          for word_props in sentence])

    return sentences


def read_sentences_gen(file_content, word2id, word_separator='\t', line_separator='\n'):
    '''
    parses sentences one-by-one (generator) out of file_content
    '''
    sentence = []
    for line in file_content.split(line_separator):
        if len(line) == 0:
            yield make_sentence(sentence, word2id)
            sentence = []
        else:
            word_props = line.split(word_separator)
            sentence.append(word_props)

    # last sentence
    if len(sentence) > 0:
        yield s


def make_sentence(raw_sentence, word2id):
    return [Word(word_props, word2id) for word_props in raw_sentence]

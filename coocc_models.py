from collections import defaultdict, Counter
from nltk.corpus import stopwords
from vectors import get_vectors
from similarity import coocc_first_order_similarity, coocc_second_order_similarity


class CoOcc(object):
    '''
    general co-occurrences model
    '''

    def __init__(self):
        self.word_context_count = defaultdict(Counter)
        self.words_having_context = defaultdict(set)
        self._vectors = None
        self._norms = None

    def _context_to_description(self, context, word2id):
        raise NotImplementedError()

    def _word_context_generator(self, sentence):
        raise NotImplementedError()

    def add_sentence(self, sentence):
        '''
        add (word, context) pairs to coocc dict from sentence
        '''
        for (word, context) in self._word_context_generator(sentence):
            context_count_for_word = self.word_context_count[word]
            context_count_for_word[context] += 1
            self.words_having_context[context].add(word)

    def calc_vectors(self, alpha=1):
        '''
        calc vectors (and norms) out of word-context co-occ dict
        '''
        self._vectors, self._norms = get_vectors(self.word_context_count,
                                                 self.words_having_context, alpha)

    def most_similar_words(self, word, word2id, top_n=20):
        '''
        second order similarity
        returns top_n most similar words to word based on cosine similarity
        '''
        word_id = word2id.get_id_by_word(word)
        if word_id not in self._vectors:
            raise Exception("out of vocabulary word {}".format(word))

        # top_n + 1 because first word = word
        r = coocc_second_order_similarity(self._vectors, self._norms, self.words_having_context,
                                          word_id, top_n + 1)
        # remove first word
        return [(word2id.get_word_by_id(word), score) for word, score in r][1:]

    def most_activating_contexts(self, word, word2id, top_n=10):
        '''
        first order similarity
        returns top_n most activating contexts to word based on cosine similarity
        '''
        word_id = word2id.get_id_by_word(word)
        if word_id not in self._vectors:
            raise Exception("out of vocabulary word {}".format(word))

        word_vector = self._vectors[word_id]
        r = coocc_first_order_similarity(word_vector, top_n)
        return [(self._context_to_description(context, word2id), score) for context, score in r]


class FullContextCoOcc(CoOcc):
    '''
    full context - all words in the sentence co-occuring the target word
    '''

    def __init__(self, word_filter, context_filter):
        super().__init__()
        self._word_filter = word_filter
        self._context_filter = context_filter

    def _context_to_description(self, context, word2id):
        return word2id.get_word_by_id(context)

    def _word_context_generator(self, sentence):
        for i, word in enumerate(sentence):
            if not self._word_filter.filter(word):
                continue
            for context in sentence[:i] + sentence[i + 1:]:
                if not self._context_filter.filter(context):
                    continue
                yield word.id, context.id


def get_function_words(word2id):
    return set((word2id.get_id_by_word(w) for w in stopwords.words('english')))


class SkipFunctionWordsWindowContextCoOc(CoOcc):
    '''
    window context - all words in the window co-occuring the target word
    skip function words - don't count for words/context which are function words
    '''

    def __init__(self, word_filter, context_filter, function_words, window_size=2):
        super().__init__()
        self._window_size = window_size
        self._function_words = function_words
        self._word_filter = word_filter
        self._context_filter = context_filter

    def _context_to_description(self, context, word2id):
        return word2id.get_word_by_id(context)

    def _word_context_generator(self, sentence):
        no_function_words_sentence = [
            w for w in sentence if w.id not in self._function_words]
        sentence = no_function_words_sentence
        for i, word in enumerate(sentence):
            if not self._word_filter.filter(word):
                continue
            window = sentence[i - self._window_size: i] + \
                sentence[i + 1: i + 1 + self._window_size]
            for context in window:
                if not self._context_filter.filter(context):
                    continue
                yield word.id, context.id

# dependency simple relation
CONTEXT_TYPE_DEP_REL = 0
# dependency prepoisition relation
CONTEXT_TYPE_PREPOSITION = 1
# word is parent of related word
DEP_REL_DIR_PARENT = 0
# word is child of related word
DEP_REL_DIR_CHILD = 1
# inverted relation symbol
INVERTED_RELATION_SYMBOL = "I"


def get_dependency_relations(sentence):
    '''
    get parent and children dict for each word in sentence
    '''
    parent = dict()
    children = defaultdict(set)

    for i, word in enumerate(sentence):
        if word.head != None and word.deprel != None:
            parent[i] = word.head
            children[word.head].add(i)

    return parent, children


class DependencyContextCoOcc(CoOcc):
    '''
    dependency context - all words with dependency relation to the word co-occuring the target word
    relation of (word <-> preposition <-> related_word) is added as (word, related_word)
    '''

    def __init__(self, word_filter, context_filter):
        super().__init__()
        self._word_filter = word_filter
        self._context_filter = context_filter

    def _context_to_description(self, context, word2id):
        context_type = context[0]
        if context_type == CONTEXT_TYPE_DEP_REL:
            relation = context[2]
            related_word = word2id.get_word_by_id(context[1])
            context_dir = INVERTED_RELATION_SYMBOL if context[
                3] == DEP_REL_DIR_CHILD else ""
            return "{}{}_{}".format(relation, context_dir, related_word)
        elif context_type == CONTEXT_TYPE_PREPOSITION:
            relation = context[2]
            context_dir = INVERTED_RELATION_SYMBOL if context[
                4] == DEP_REL_DIR_CHILD else ""
            preposition = word2id.get_word_by_id(context[3])
            related_word = word2id.get_word_by_id(context[1])
            return "{}:{}{}_{}".format(relation, preposition, context_dir, related_word)
        else:
            raise Exception("unknown context type {}".format(context_type))

    def _word_context_generator(self, sentence):
        parent, children = get_dependency_relations(sentence)
        for i, word in enumerate(sentence):
            if not self._word_filter.filter(word):
                continue
            # target_word -> parent
            if i in parent:  # skip words without head (ROOT)
                parent_word = sentence[parent[i]]
                if self._context_filter.filter(parent_word):
                    dep_edge = (CONTEXT_TYPE_DEP_REL, parent_word.id,
                                word.deprel, DEP_REL_DIR_PARENT)
                    yield word.id, dep_edge

                # preposition
                if parent_word.is_preposition() and parent_word.head != None:
                    related_word = sentence[parent_word.head]
                    if self._context_filter.filter(related_word):
                        dep_edge = (CONTEXT_TYPE_PREPOSITION, related_word.id,
                                    parent_word.deprel, parent_word.id, DEP_REL_DIR_PARENT)
                        yield word.id, dep_edge

            # child -> target_word
            for child in children[i]:
                child_word = sentence[child]
                if self._context_filter.filter(child_word):
                    dep_edge = (CONTEXT_TYPE_DEP_REL, child_word.id,
                                child_word.deprel, DEP_REL_DIR_CHILD)
                    yield word.id, dep_edge

                # preposition
                if child_word.is_preposition():
                    for grand_child in children[child]:
                        grand_child_word = sentence[grand_child]
                        if grand_child_word.is_noun():
                            related_word = grand_child_word
                            if self._context_filter.filter(related_word):
                                dep_edge = (CONTEXT_TYPE_PREPOSITION, related_word.id,
                                            child_word.deprel, child_word.id, DEP_REL_DIR_CHILD)
                                yield word.id, dep_edge

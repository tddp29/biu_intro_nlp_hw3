import bz2
from io import BytesIO
import numpy as np


def load_model(file_path):
    '''
    returns matrix, index2word, word2index
    '''
    i2w = []
    w2i = {}
    m = []
    with open(file_path, 'rb') as f:
        s = BytesIO(f.read())
    with bz2.BZ2File(s) as bzf:
        for i, line in enumerate(bzf):
            v = line.split()
            word = v[0].decode('utf-8')
            w2i[word] = i
            i2w.append(word)
            m.append(np.array(v[1:], dtype=np.float))
    matrix = np.array(m)

    return matrix, i2w, w2i


def word2vec_most_similar_words(word_matrix, i2w, word_vec, top_n=20):
    '''
    calc top_n most similar word to word based cosine similarity
    note: removes first word (which is the word itself with similarity=1)
    returns list of (word, similarity)
    '''
    sims = np.dot(word_matrix, word_vec)
    # get last top_n removing first one which is the word itself
    most_similar_indeces = np.argsort(sims)[-(top_n + 1):-1][::-1]

    return [(i2w[ind], sims[ind]) for ind in most_similar_indeces]


def word2vec_most_activating_contexts(context_matrix, i2c, word_vec, top_n=10):
    '''
    calc top_n most activating contexts of word based cosine similarity
    returns list of (context, similarity)
    '''
    sims = np.dot(context_matrix, word_vec)

    most_similar_indeces = np.argsort(sims)[-top_n:][::-1]

    return [(i2c[ind], sims[ind]) for ind in most_similar_indeces]

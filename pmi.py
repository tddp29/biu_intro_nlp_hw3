import math


def pmi(all_word_context_count, word_context_count, word_count, context_count):
    '''
    pmi and pmi_estimation for (word, context) where word or context are "constant"

    pmi calc explained:

       p(w,c)                 #(w,c) / #(*,*)               #(*,*)     #(w,c)
    ---------- = --------------------------------------- = -------- * --------
     p(w)*p(c)    (#(w,*) / #(*,*)) * (#(*,c) / #(*,*))     #(w,*)     #(*,c)
    '''
    return math.log2(all_word_context_count * word_context_count / (word_count * context_count))
